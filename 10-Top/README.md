# opi-rfq-SystemOwner

OPI content for RFQ to be used by System Owners on the control room workstations.

Content can be added by anyone via merge request. Approval of merge request is by the repository admin/owner.
The repo owner will request check that the content has been reviewed by someone with an interest in the system. Repository owner will also check that the conventions below have been followed for house-keeping purposes.

## Conventions
10-Top directory is for screens that can be opened directly
99-Shared directory is for objects that are called by/embedded in other screens.
