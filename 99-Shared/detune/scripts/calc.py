from org.csstudio.display.builder.runtime.script import PVUtil

# Cases:
# 1 : rms
# 2 : p2p
# 3 : mean

case = PVUtil.getDouble(pvs[4])

def convTimeToInd(arr,val): # returns the index of the value closest
    return min(range(len(arr)), key=lambda i: abs(arr[i]-val))

def getROIarray(arr,start,end):
    return arr[start:end]

def mean(arr):
    return sum(arr)/len(arr)

def arr_square(arr):
    arr_out = []
    for y in arr:
        arr_out.append(y**2)
    return arr_out

array = PVUtil.getDoubleArray(pvs[0])
arrayTime = PVUtil.getDoubleArray(pvs[1])

ROIstart = convTimeToInd(arrayTime,PVUtil.getDouble(pvs[2]))
ROIend = convTimeToInd(arrayTime,PVUtil.getDouble(pvs[3]))

realArray = getROIarray(array,ROIstart,ROIend)

if case == 1: # rms
    res = mean(arr_square(realArray))**(0.5)
elif case == 2: # p2p
    res = (mean(realArray)-min(realArray))/(2*mean(realArray)) if mean(realArray) != 0 else 0
elif case == 3: # mean
    res = mean(realArray)

widget.setPropertyValue('text', round(res,4))
