import runner, argparse

modes = ["local","test"] # 0, 1, 2 ; test = using epics for getting val, printing write values
mode = modes[1]

parser = argparse.ArgumentParser(description = "Start up the RFQ from HVON to RFON and auto-reset")
parser.add_argument("power",       type = float, help = "Power [kW]")
parser.add_argument("pulselength", type = float, help = "Pulse length [us]")
parser.add_argument("reprate",     type = float, help = "Repetition rate [Hz]")

parser.add_argument("--no-startup",  action = 'store_true', help = "Assume the system is up when starting the script")

parser.add_argument("--stable-detuning-tolerance", type = float, default = 5000, help = "Detuning in Hz around 0 Hz considered stable for operation without tracking and closed loop operation")
parser.add_argument("--stable-detuning-time",      type = float, default = 60,   help = "Time the detuning needs to be withing value given by --stable-detuning-tolerance")

parser.add_argument("--close-loop",  type = float, help = "Close the loop after startup around given voltage [kV]")

args = parser.parse_args()

runner.exec_rst(mode,args)



# global fim_state, il_time, times_fast_il
