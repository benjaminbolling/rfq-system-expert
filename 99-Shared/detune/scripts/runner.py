from time import sleep, time
from collections import deque

import start_Params as SP
import IO

def run_full_reset(mode):
    printerFunc(mode,"{} ### Full reset ###".format(time()))
    printerFunc(mode,"{} Open LLRF loop".format(time()))
    write_PV(SP.pvs_dict()["pv_DIG1_OpenLoop"], 1)
    write_PV(SP.pvs_dict()["pv_DIG1_RFCtrlCnstFFEn"], 0)
    printerFunc(mode,"{} Switch LLRF tracking on".format(time()))
    write_PV(SP.pvs_dict()["pv_LLRF_power"], 1)

    # Stop LLRF
    printerFunc(mode,"{} Rearm LLRF".format(time()))
    for pv in SP.pvs_dict()["pv_DIG_Msg_lst"]:
        write_PV(pv, "RESET")
    sleep(0.01)
    for pv in SP.pvs_dict()["pv_DIG_Msg_lst"]:
        write_PV(pv, "INIT")

    # Put startup settings
    ramp_state = get_PV(mode,pvs_dict()["locpv_rampstate"])
    set_reprate, set_pulselength, set_power, _ = get_ramp_table(mode)[int(ramp_state)]

    printerFunc(mode,"{}: Startup settings: {} Hz, {} us, {} kW".format(time(), set_reprate, set_pulselength, set_power))
    write_PV(SP.pvs_dict()["pv_RFS_cycleFreq"], set_reprate)
    write_PV(SP.pvs_dict()["pv_RFS_RFSyncWdt"], set_pulselength)
    write_PV(SP.pvs_dict()["pv_LLRF_power"], set_power)

    # Reset SIM & FIM
    printerFunc(mode,"{}: Reset SIM".format(time()))
    write_PV(SP.pvs_dict()["pv_RFS_CPU_Rst"], 1)
    sleep(0.1)
    write_PV(SP.pvs_dict()["pv_RFS_CPU_Rst"], 1)
    write_PV(SP.pvs_dict()["pv_RFS_CPU_RFON"], 1)

    conda = get_PV(mode,SP.pvs_dict()["pv_RF_precond"]) == 1
    condb = get_PV(mode,SP.pvs_dict()["pv_RF_RFENMISUCND"]) == 1
    condc = get_PV(mode,SP.pvs_dict()["pv_CPU_state"]) == 5
    i = 0

    while(not(conda and condb and condc)): # TODO : get rid of ...
        printerFunc(mode,"{} Reset ILs {}".format(time(), i))
        write_PV(SP.pvs_dict()["pv_RFS_FIM_Rst"], 1)
        write_PV(SP.pvs_dict()["pv_RFS_FIM_Rst"], 1)

        printerFunc(mode,"{}: Reset FIM {}".format(time(), i))
        write_PV(SP.pvs_dict()["pv_RFS_FSM"], "RFON")

        sleep(0.5)
        conda = get_PV(mode,SP.pvs_dict()["pv_RF_precond"]) == 1
        condb = get_PV(mode,SP.pvs_dict()["pv_RF_RFENMISUCND"]) == 1
        condc = get_PV(mode,SP.pvs_dict()["pv_CPU_state"]) == 5

        i += 1

    # Restart LLRF
    printerFunc(mode,"{}: Restart LLRF".format(time()))
    sleep(0.01)
    for pv in SP.pvs_dict()["pv_DIG_Msg_lst"]:
        write_PV(pv, "ON")

def exec_rst(mode,args):
    if mode == "local" or mode == "test":
        args = IO.conv_inp_args(mode,args)
        global state
        state = SP.stateDict()["Ramping"]
        # import epics
        # epics.camonitor(SP.pvs_dict()["pv_fim_state"],callback=pp)
        # epics.camonitor(SP.pvs_dict()["pv_RF_precond"],callback=pp)
        # epics.camonitor(SP.pvs_dict()["pv_RF_RFENMISUCND"],callback=pp)
        # epics.camonitor(SP.pvs_dict()["pv_CPU_state"],callback=pp)
        # epics.camonitor(SP.pvs_dict()["pv_Detune_PhaseShift_MovAvg"],callback=pp)

        # epics.camonitor(SP.pvs_dict()["pv_Detune_PhaseShift_MovAvg"],callback = monitor_detuning)
        while True:
            sleep(1)
            reset_Loop(mode,args)
    elif mode == "phoebus":
        locpvs,args = [],{}
        for k,v in SP.pvs_dict():
            if "locpv_" in k:
                args[k] = get_PV(mode,v)
        reset_Loop(mode,args)

def construct_auto_ramptable():
    # get the values defined in the OPI
    tab = []
    types = ["reprate","pulselength","power"]
    for type in types:
        vals = []
        for var in ["start","step","goal"]:
            vals.append(get_PV(SP.pvs_dict()["locpv_{}_{}".format(type,var)]))
        tab.append(vals)

    steptime = get_PV(SP.pvs_dict()["steptime"])
    plateutime = get_PV(SP.pvs_dict()["plateutime"])

    # create a table with rows containing the levels for each type
    table = []
    for i in range(len(types)):
        start,step,goal,steptime,plateutime = tab[i]

        subtable = []
        for lvl in range(start,goal,step):
            subtable.append(lvl)
        if subtable[-1] < goal:
            subtable.append(goal)

        table.append(subtable)

    # construct the final table
    master_table = []
    for reprate in table[0]:
        for pulselength in table[1]:
            for power in table[2]:
                master_table.append([reprate,pulselength,power,steptime])
    return master_table

def get_ramp_table(mode):
    if mode == "phoebus":
        return construct_auto_ramptable()
    else:
        return SP.get_ramp_table()

def check_for_interlock(mode):
    if get_PV(mode,SP.pvs_dict()["pv_fim_state"]) == "RFON":
        return False
    else:
        return True

    # if(il_time is None):
    #     return False
    # il = False
    # # callback_lock = Lock()
    # # callback_lock.acquire()
    # if(fim_state == FIM_HVON): # TODO : find the values ...
    #     time_since_il = abs(time() - il_time)
    #     print("Time to reset: {}".format(il_deadtime - time_since_il))
    #     if(time_since_il > il_deadtime):
    #         il = True
    # # callback_lock.release()
    # return il
    # return True # interlock reset needed

def reset_Loop(mode,args):
    state = get_PV(mode,SP.pvs_dict()["locpv_state"])

    if(state == SP.stateDict()["Nominal"]): # Check need for reset
        if(check_for_interlock(mode)):
            stateChange(mode,"Reset")
    elif(state == SP.stateDict()["Reset"]): # Run reset
        run_full_reset(mode)
        if(check_for_interlock()):
            stateChange(mode,"Reset")
        else:
            stateChange(mode,"Ramping")
    elif(state == SP.stateDict()["Ramping"]): # Start ramping
        # Check need for reset (if system is down again)
        if(check_for_interlock(mode)):
            stateChange(mode,"Reset")

        # Reached end of ramp: back to nominal state
        elif(len(actual_ramp) == 0):
            stateChange(mode,"NominalStartup")

        # Apply new setting if ok
        else:
            present_time = time()

            # Don't apply new setting if there has been a fast IL at that setting
            if(ramp_wait_time is not None):
                il_in_waittime = any([(present_time - t) < ramp_wait_time for t in times_fast_il])
                if(il_in_waittime):
                    printerFunc(mode,"{}: IL on this setting. Wait longer.".format(time()))
                    ramp_begin_time = time()
            else:
                il_in_waittime = False

            # Apply setting if enough time passed
            if(ramp_begin_time is None or ramp_wait_time is None or ((present_time - ramp_begin_time) > ramp_wait_time)):
                reprate, pulselength, power, ramp_wait_time = actual_ramp.popleft()

                set_reprate     = min(reprate,     args["reprate"])
                set_pulselength = min(pulselength, args["pulselength"])
                set_power       = min(power,       args["power"])
                printerFunc(mode,"{}: Ramp {} Hz, {} us, {} kW".format(time(), set_reprate, set_pulselength, set_power))

                write_PV(SP.pvs_dict()["pv_RFS_cycleFreq"], set_reprate)
                write_PV(SP.pvs_dict()["pv_RFS_RFSyncWdt"], set_pulselength)
                write_PV(SP.pvs_dict()["pv_LLRF_power"], set_power)

                ramp_begin_time = time()
    elif(state == SP.stateDict()["NominalStartup"]): # Nominal (waiting for frequency to be stable)
        # Check need for reset (if system is down again)
        if(check_for_interlock()):
            stateChange(mode,"Reset")

        if(all(abs(f) < args["stable_detuning_tolerance"] for f in detuning_buffer)):
            printerFunc(mode,"{} Switch LLRF tracking off".format(time()))
            write_PV(SP.pvs_dict()["pv_LLRF_freqTrak"], 0)

            power = get_PV(mode,SP.pvs_dict()["pv_LLRF_power"])
            write_PV(SP.pvs_dict()["pv_LLRF_power"], power+0.01)
            sleep(0.01)
            write_PV(SP.pvs_dict()["pv_LLRF_power"], power)

            if mode == "phoebus":
                if get_PV(mode,SP.pvs_dict()["locpv_close_loop"]) == 1:
                    close_loop(mode)
            elif(args["close_loop"]):
                close_loop(mode)

            stateChange(mode,"Nominal")

def close_loop(mode):
    printerFunc(mode,"{} Switch LLRF feedback on".format(time()))
    write_PV(SP.pvs_dict()["pv_DIG_OpenLoop"], 0)

def stateChange(mode,state_goal):
    if mode == "phoebus":
        write_PV(SP.pvs_dict()["locpv_state"], SP.stateDict()[state_goal])
    else:
        state = SP.stateDict()[state_goal]

def printerFunc(mode,val):
    if mode == "phoebus":
        write_PV(mode,SP.pvs_dict()["locpv_logger"],val)
    else:
        print(val)

def get_PV(mode,pv):
    if mode == "phoebus":
        pv = PVFactory.getPV(pv)
        try:
            val = PVUtil.getDouble(pv)
        except:
            val = PVUtil.getString(pv)
        return val
    else:
        import epics
        return epics.caget(pv)

def write_PV(mode,pv,val):
    if mode == "phoebus":
        try:
            PVUtil.writePV(PVFactory.getPV(pv),val,10)
        except:
            msg = ":: Error :: Failed to write to PV {} --> {}".format(pv, val)
            printerFunc(mode,msg)
    else:
        import epics
        if mode == "local": epics.caput(pv,val)
        elif mode == "test": print("{} --> {}".format(pv,val))


# def pp(pvname=None, value=None, mode, **kw):
#     printerFunc(mode,"{}: {} -> {}".format(kw["timestamp"], pvname, value))

# def monitor_detuning(pvname=None, value=None, char_value=None, **kw):
#     detuning_buffer.append(value)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                             #
#                                                                             #
# Beneath: Only included in the embedded file - required by Jython in Phoebus #
#                                                                             #
#                                                                             #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
from org.csstudio.display.builder.runtime.script import PVUtil
from org.csstudio.display.builder.runtime.pv import PVFactory

mode = "phoebus"

globalrun = PVUtil.getDouble(pvs[0])
localrun = PVUtil.getDouble(pvs[1])
localrunBU = PVUtil.getDouble(pvs[2])

# --------------------------------------------------------------------------- #
# # # # This segment is for ensuring this OPI only runs at one location # # # #

if globalrun == 0 and localrun == 1:
    print("running locally")
    exec_rst(mode,None)
elif globalrun == 1 and localrun == 0:
    print("not running")
elif globalrun == 0 and localrun == 0 and localrunBU == 1:
    print("Was running here, stopping")
    PVUtil.writePV(pvs[0],1,5) # Re-enabling the global PV
    PVUtil.writePV(pvs[2],0,5) # Resetting the local backup PV
elif globalrun == 0 and localrun == 0 and localrunBU == 0:
    print("running elsewhere")
elif globalrun == 1 and localrun == 1 and localrunBU == 1:
    PVUtil.writePV(pvs[0],0,5) # Re-disabling the global PV
    ar_IO.execute_reset()
elif globalrun == 1 and localrun == 1 and localrunBU == 0:
    print("Starting script")
    PVUtil.writePV(pvs[0],0,5) # Disabling the global PV
    PVUtil.writePV(pvs[2],1,5) # Activating the local backup PV


# --------------------------------------------------------------------------- #


# --------------------------------------------------------------------------- #
# Exiting : Ensure always the global run is reset in case one closes the OPI
if localrun == 1 or localrunBU == 1:
    PVUtil.writePV(pvs[0],1,5)
# --------------------------------------------------------------------------- #
