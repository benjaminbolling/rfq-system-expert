#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import deque

def get_parameters(args=None):
    if args is None: args = {"stable_detuning_time" : 5000}
    return {
            "il_deadtime" : 2,
            "FIM_HVON" : 2,
            "FIM_RFON" : 3,
            "fim_state" : get_PV(pvs_dict["pv_fim_state"]),
            "il_time" : None,
            "times_fast_il" : deque(maxlen = 500),
            "detuning_buffer" : deque(maxlen = int(14 * args["stable_detuning_time"])),
            "ramp_begin_time" : None,
            "ramp_wait_time" : None,
            }

def get_ramp_table():
    return [( 1,   50,   50, 30),
            ( 1,  200,  500, 10),
            ( 1, 1600,  500, 10),
            ( 1, 3200,  500, 10),
            ( 1, 3200,  600, 10),
            ( 4, 3200,  600, 10),
            ( 8, 3200,  600, 10),
            (12, 3200,  600, 10),
            (14, 3200,  600, 20),
            (14, 3200,  700, 20),
            (14, 3200,  830, 20)]

def stateDict():
    return {
            "Reset" : 1,
            "Nominal" : 2,
            "Ramping" : 3,
            "NominalStartup" : 4
            }

def pvs_dict():
    return {
            "pv_fim_state" : "RFQ-010:RFS-FIM-101:FSM-RB",
            "pv_RF_precond" : "RFQ-010:RFS-FIM-101:RFON-Precond",
            "pv_RF_RFENMISUCND" : "RFQ-010:RFS-CPU-RFENMISUCND-RB",
            "pv_CPU_state" : "RFQ-010:RFS-CPU-STATE-RB",
            "pv_Detune_PhaseShift_MovAvg" : "RFQ-LLRF1::DetunePhaShftMovAvg",
            "pv_VaneLoopMode" : "CWM-CWS03::VaneLoopMode-S",
            "pv_DIG_OpenLoop" : "RFQ-010:RFS-DIG-101:OpenLoop",
            "pv_RFS_cycleFreq" : "RFQ-010:RFS-EVR-101:CycleFreq-SP",
            "pv_RFS_RFSyncWdt" : "RFQ-010:RFS-EVR-101:RFSyncWdt-SP",
            "pv_LLRF_power" : "RFQ-LLRF1::FFPulseGenP",
            "pv_LLRF_freqTrak" : "RFQ-LLRF1::FFFreqTrackEn",
            "pv_DIG_Msg_lst" : ["RFQ-010:RFS-DIG-10{}:Msg".format(i) for i in range(1,5)],
            "pv_DIG1_OpenLoop" : "RFQ-010:RFS-DIG-101:OpenLoop",
            "pv_DIG1_RFCtrlCnstFFEn" : "RFQ-010:RFS-DIG-101:RFCtrlCnstFFEn",
            "pv_RFS_CPU_RFON" : "RFQ-010:RFS-CPU-RFON",
            "pv_RFS_CPU_Rst" : "RFQ-010:RFS-CPU-RST",
            "pv_RFS_FIM_Rst" : "RFQ-010:RFS-FIM-101:Rst",
            "pv_RFS_FSM" : "RFQ-010:RFS-FIM-101:FSM",
            "pv_script_enabled" : "RFQ-010::PhaseDetuneEn"
            ####################################################################
            "locpv_logger" : "loc://rfq_phs_detune_exp_opi_logger",
            "locpv_state" : "loc://rfq_phs_detune_exp_opi_stable_detuning_state",
            "locpv_rampstate" : "loc://rfq_phs_detune_exp_opi_stable_detuning_rampstate",

            "locpv_steptime" : "loc://rfq_phs_detune_exp_opi_steptime",
            "locpv_plateutime" : "loc://rfq_phs_detune_exp_opi_plateutime",

            "locpv_stable_detuning_time" : "loc://rfq_phs_detune_exp_opi_stable_detuning_time",
            "locpv_stable_detuning_tolerance" : "loc://rfq_phs_detune_exp_opi_stable_detuning_tolerance",
            "locpv_close_loop" : "loc://rfq_phs_detune_exp_opi_close_loop",

            "locpv_reprate_start" : "loc://rfq_phs_detune_exp_opi_reprate_start",
            "locpv_pulselength_start" : "loc://rfq_phs_detune_exp_opi_pulselength_start",
            "locpv_power_start" : "loc://rfq_phs_detune_exp_opi_power_start",

            "locpv_reprate_step" : "loc://rfq_phs_detune_exp_opi_reprate_step",
            "locpv_pulselength_step" : "loc://rfq_phs_detune_exp_opi_pulselength_step",
            "locpv_power_step" : "loc://rfq_phs_detune_exp_opi_power_step",

            "locpv_reprate_goal" : "loc://rfq_phs_detune_exp_opi_reprate_goal",
            "locpv_pulselength_goal" : "loc://rfq_phs_detune_exp_opi_pulselength_goal",
            "locpv_power_goal" : "loc://rfq_phs_detune_exp_opi_power_goal",
            }
