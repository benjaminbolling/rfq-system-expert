import epics
import argparse
from time import sleep, time
from collections import deque
from threading import Lock
from enum import Enum

parser = argparse.ArgumentParser(description = "Start up the RFQ from HVON to RFON and auto-reset")
parser.add_argument("power",       type = float, help = "Power [kW]")
parser.add_argument("pulselength", type = float, help = "Pulse length [us]")
parser.add_argument("reprate",     type = float, help = "Repetition rate [Hz]")

parser.add_argument("--no-startup",  action = 'store_true', help = "Assume the system is up when starting the script")

parser.add_argument("--stable-detuning-tolerance", type = float, default = 5000, help = "Detuning in Hz around 0 Hz considered stable for operation without tracking and closed loop operation")
parser.add_argument("--stable-detuning-time",      type = float, default = 60,   help = "Time the detuning needs to be withing value given by --stable-detuning-tolerance")

parser.add_argument("--close-loop",  type = float, help = "Close the loop after startup around given voltage [kV]")

args = parser.parse_args()

ramp = [( 1,   50,   50, 30),
        ( 1,  200,  500, 10),
        ( 1, 1600,  500, 10),
        ( 1, 3200,  500, 10),
        ( 1, 3200,  600, 10),
        ( 4, 3200,  600, 10),
        ( 8, 3200,  600, 10),
        (12, 3200,  600, 10),
        (14, 3200,  600, 20),
        (14, 3200,  700, 20),
        (14, 3200,  830, 20)]
il_deadtime = 2

FIM_HVON = 2
FIM_RFON = 3

callback_lock = Lock()

#####################################
######## Monitor FSM for ILs ########
#####################################
fim_state = epics.caget("RFQ-010:RFS-FIM-101:FSM-RB")
il_time = None
times_fast_il = deque(maxlen=500)
def reset_full_il(pvname=None, value=None, char_value=None, **kw):
  global fim_state, il_time, times_fast_il
  print("{}: {} -> {}".format(kw["timestamp"], pvname, value))

  callback_lock.acquire()
  if(value == FIM_HVON and fim_state == FIM_RFON):
    il_time = time()

  if(value == FIM_RFON and fim_state == FIM_HVON and il_time is not None):
    time_since_il = abs(time() - il_time)
    if(time_since_il < 1.1):
      times_fast_il.append(il_time)

    il_time = None

  fim_state = value
  callback_lock.release()
epics.camonitor("RFQ-010:RFS-FIM-101:FSM-RB",       callback=reset_full_il)

def check_for_interlock():
  if(il_time is None):
    return False

  il = False

  callback_lock.acquire()
  if(fim_state == FIM_HVON):
    time_since_il = abs(time() - il_time)
    print("Time to reset: {}".format(il_deadtime - time_since_il))
    if(time_since_il > il_deadtime):
      il = True
  callback_lock.release()

  return il

#######################################
######## Monitor for debugging ########
#######################################
def pp(pvname=None, value=None, char_value=None, **kw):
  print("{}: {} -> {}".format(kw["timestamp"], pvname, value))

epics.camonitor("RFQ-010:RFS-FIM-101:RFON-Precond", callback=pp)
epics.camonitor("RFQ-010:RFS-CPU-RFENMISUCND-RB",   callback=pp)
epics.camonitor("RFQ-010:RFS-CPU-STATE-RB",         callback=pp)

#######################################
######## Monitor detuning #############
#######################################
#TODO: assumes 14 Hz as target for ramp
detuning_buffer = deque(maxlen = int(14 * args.stable_detuning_time))
def monitor_detuning(pvname=None, value=None, char_value=None, **kw):
  detuning_buffer.append(value)

epics.camonitor("RFQ-LLRF1::DetunePhaShftMovAvg", callback = monitor_detuning)

#######################################
######## LLRF/LPS reset ###############
#######################################
def run_full_reset():
  print("{} ### Full reset ###".format(time()))
  print("{} Open LLRF loop".format(time()))
  epics.caput("RFQ-010:RFS-DIG-101:OpenLoop", 1)
  epics.caput("RFQ-010:RFS-DIG-101:RFCtrlCnstFFEn", 0)
  print("{} Switch LLRF tracking on".format(time()))
  epics.caput("RFQ-LLRF1::FFFreqTrackEn", 1)

  # Stop LLRF
  print("{} Rearm LLRF".format(time()))
  for i in range(1,5):
    epics.caput("RFQ-010:RFS-DIG-10{}:Msg".format(i), "RESET")
  sleep(0.01)
  for i in range(1,5):
    epics.caput("RFQ-010:RFS-DIG-10{}:Msg".format(i), "INIT")

  # Put startup settings
  set_reprate, set_pulselength, set_power, _ = ramp[0]
  print("{}: Startup settings: {} Hz, {} us, {} kW".format(time(), set_reprate, set_pulselength, set_power))
  epics.caput("RFQ-010:RFS-EVR-101:CycleFreq-SP", set_reprate)
  epics.caput("RFQ-010:RFS-EVR-101:RFSyncWdt-SP", set_pulselength)
  epics.caput("RFQ-LLRF1::FFPulseGenP",           set_power)

  # Reset SIM & FIM
  print("{}: Reset SIM".format(time()))
  epics.caput("RFQ-010:RFS-CPU-RST", 1)
  sleep(0.1)
  epics.caput("RFQ-010:RFS-CPU-RST", 1)
  epics.caput("RFQ-010:RFS-CPU-RFON", 1)

  conda = epics.caget("RFQ-010:RFS-FIM-101:RFON-Precond") == 1
  condb = epics.caget("RFQ-010:RFS-CPU-RFENMISUCND-RB") == 1
  condc = epics.caget("RFQ-010:RFS-CPU-STATE-RB") == 5
  i = 0
  while(not(conda and condb and condc)):
    print("{} Reset ILs {}".format(time(), i))
    epics.caput("RFQ-010:RFS-FIM-101:Rst", 1)
    epics.caput("RFQ-010:RFS-FIM-101:Rst", 1)

    print("{}: Reset FIM {}".format(time(), i))
    epics.caput("RFQ-010:RFS-FIM-101:FSM", "RFON")

    sleep(0.5)
    conda = epics.caget("RFQ-010:RFS-FIM-101:RFON-Precond") == 1
    condb = epics.caget("RFQ-010:RFS-CPU-RFENMISUCND-RB") == 1
    condc = epics.caget("RFQ-010:RFS-CPU-STATE-RB") == 5

    i += 1

  # Restart LLRF
  print("{}: Restart LLRF".format(time()))
  sleep(0.01)
  for i in range(1,5):
    epics.caput("RFQ-010:RFS-DIG-10{}:Msg".format(i), "ON")

#######################################
######## State machine ################
#######################################
class State(Enum):
  Reset          = 1
  Nominal        = 2
  Ramping        = 3
  NominalStartup = 4

if(args.no_startup):
  state = State.Nominal
else:
  state = State.Reset

ramp_begin_time = None
ramp_wait_time = None
while(True):
  sleep(1)

  ######## State machine: Nominal ################
  if(state == State.Nominal):
    # Check need for reset
    if(check_for_interlock()):
      state = State.Reset

  ######## State machine: Reset ################
  elif(state == State.Reset):
    # Run reset
    run_full_reset()

    # Check that state machine is back up
    if(il_time == None):
      actual_ramp = deque(ramp)
      state = State.Ramping

  ######## State machine: Ramping ################
  elif(state == State.Ramping):
    # Check need for reset (if system is down again)
    if(check_for_interlock()):
      state = State.Reset
      continue

    # Reached end of ramp: back to nominal state
    if(len(actual_ramp) == 0):
      state = State.NominalStartup
      epics.caput("CWM-CWS03::VaneLoopMode-S", 0)
      ramp_wait_time = None
    # Apply new setting if ok
    else:
      present_time = time()

      # Don't apply new setting if there has been a fast IL at that setting
      if(ramp_wait_time is not None):
        il_in_waittime = any([(present_time - t) < ramp_wait_time for t in times_fast_il])
        if(il_in_waittime):
          print("{}: IL on this setting. Wait longer.".format(time()))
          ramp_begin_time = time()
      else:
        il_in_waittime = False

      # Apply setting if enough time passed
      if(ramp_begin_time is None or ramp_wait_time is None or ((present_time - ramp_begin_time) > ramp_wait_time)):
        reprate, pulselength, power, ramp_wait_time = actual_ramp.popleft()

        set_reprate     = min(reprate,     args.reprate)
        set_pulselength = min(pulselength, args.pulselength)
        set_power       = min(power,       args.power)
        print("{}: Ramp {} Hz, {} us, {} kW".format(time(), set_reprate, set_pulselength, set_power))

        epics.caput("RFQ-010:RFS-EVR-101:CycleFreq-SP", set_reprate)
        epics.caput("RFQ-010:RFS-EVR-101:RFSyncWdt-SP", set_pulselength)
        epics.caput("RFQ-LLRF1::FFPulseGenP",           set_power)

        ramp_begin_time = time()

  ######## State machine: Nominal (waiting for frequency to be stable) ################
  elif(state == State.NominalStartup):
    # Check need for reset (if system is down again)
    if(check_for_interlock()):
      state = State.Reset
      continue

    if(all(abs(f) < args.stable_detuning_tolerance for f in detuning_buffer)):
      print("{} Switch LLRF tracking off".format(time()))
      epics.caput("RFQ-LLRF1::FFFreqTrackEn", 0)
      state = State.Nominal

      power = epics.caget("RFQ-LLRF1::FFPulseGenP")
      epics.caput("RFQ-LLRF1::FFPulseGenP", power+0.01)
      sleep(0.01)
      epics.caput("RFQ-LLRF1::FFPulseGenP", power)

      if(args.close_loop):
        print("{} Switch LLRF feedback on".format(time()))
        #epics.caput("RFQ-LLRF1::FFPulseGenP", 0)
        #sleep(1)
        epics.caput("RFQ-010:RFS-DIG-101:OpenLoop", 0)
