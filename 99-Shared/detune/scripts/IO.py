#!/usr/bin/env python
# -*- coding: utf-8 -*-

from time import sleep, time
import start_Params as SP
# from threading import Lock

def reset_full_il(fim_state, il_time, times_fast_il,FIM_HVON,FIM_RFON):
    if(value == FIM_HVON and fim_state == FIM_RFON):
        il_time = time()

    if(value == FIM_RFON and fim_state == FIM_HVON and il_time is not None):
        time_since_il = abs(time() - il_time)
        if(time_since_il < 1.1):
            times_fast_il.append(il_time)
        il_time = None
    fim_state = value

def conv_inp_args(mode,args):
    if mode == "local" or "test":
        out_args = {
                    "stable_detuning_time" : args.stable_detuning_time,
                    "reprate" : args.reprate,
                    "pulselength" : args.pulselength,
                    "power" : args.power,
                    "stable_detuning_tolerance" : args.stable_detuning_tolerance,
                    "close_loop" : args.close_loop,
                    }
    else: out_args = args
    return out_args
